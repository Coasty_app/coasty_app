# import pygame module in this program 
import pygame 
import io

try:
    # Python2
    from urllib2 import urlopen
except ImportError:
    # Python3
    from urllib.request import urlopen
  
# activate the pygame library . 
# initiate pygame and give permission 
# to use pygame's functionality. 
pygame.init() 
  
# define the RGB value 
# for white colour 
white = (255, 255, 255) 
  
# assigning values to X and Y variable 
X = 336
Y = 723
  
# create the display surface object 
# of specific dimension..e(X, Y). 
display_surface = pygame.display.set_mode((X, Y )) 
  
# set the pygame window name 
pygame.display.set_caption('Image') 

# on a webpage right click on the image you want and use Copy image URL
image_url = "https://imagizer.imageshack.com/img922/2433/Qi7Jaq.jpg"
image_str = urlopen(image_url).read()
# create a file object (stream)
image_file = io.BytesIO(image_str)
# create a surface object, image is drawn on it. 
image = pygame.image.load(image_file)
  
# infinite loop 
while True : 
  
    # completely fill the surface object 
    # with white colour 
    display_surface.fill(white) 
  
    # copying the image surface object 
    # to the display surface object at 
    # (0, 0) coordinate. 
    display_surface.blit(image, (0, 0)) 
  
    # iterate over the list of Event objects 
    # that was returned by pygame.event.get() method. 
    for event in pygame.event.get() : 
  
        # if event object type is QUIT 
        # then quitting the pygame 
        # and program both. 
        if event.type == pygame.QUIT : 
  
            # deactivates the pygame library 
            pygame.quit() 
  
            # quit the program. 
            quit() 
  
        # Draws the surface object to the screen.   
        pygame.display.update()  