![Jxref](https://imagizer.imageshack.com/img922/116/AvGc6M.png)
# Coasty Mobile Developer README

Coasty is a live fish marketing app made to give aquarium enthusiasts and other sea-life connoisseurs a place to care for their own exotic fish. Download the app today and bring your new friend home with you. 

## Installation
### Beforehand
1. Download Python coding language through [Python's official website](https://www.python.org/downloads/) and follow the setup to install Python into PATH, pip installer, and IDLE editor.
2. Using command line terminal, install **pygame** and **pyinstaller** to your device
```bash
pip install pygame
pip install pyinstaller
```
3. After installing, check that the 

### Download the Mobile App's Pygame Source code
1. Download the files from the git repository through your command line.
```bash
git clone https://Coasty@bitbucket.org/Coasty_app/coasty_app.git
```
### Download the Mobile App's Landing Page source code
1. Visit the [kind of official repl.it](https://repl.it/@dariustb/Coasty-Landing-Page) to have access to the latest version of the Landing page's code. The landing page also includes the executable of the app and the user & developer readme file included.
2. Click the **...** dropdown menu on the far right of the "File" header.
3. Select **Download as zip** to have the files packed in a zipped folder.


## Usage
### Executable
![Coastly App](https://imagizer.imageshack.com/v2/640x480q90/924/IGWM37.png)

1. First open the file and wait for it to load.
2. After loading, take the time to appreciate the appearance of the mobile app, because the development is skeletal and virtually non-functioning.
3. Close the application, then take a deep look at your actions that had led you to downloading the file.

### Pygame configuration
1. After downloading the pygame module to your device, open your preferred code editor or IDE
2. Open this file in the editor and compile the code for the app to run.


## Contributing

Contributions and suggestions for improvements are welcome. For major changes, please contact me via [email](mailto:coasty@protonmail.com) to discuss what you would like to change. However, this project is fictional and I may not respond.

Please make sure to update tests as appropriate.

## Notes

 - The website and documents are fake and are created to fulfill the
   necessary instructions for assignment 4.
  - If you're outside of the class, seeing this, drop a positive comment about the fake app. I might get bonus points

## Credits

This project is completed by Darius Brown for CSC 435 Software Engineering with prof. Cherry.

## License

[(c) 2020 Darius Brown Astley Labs Software License](https://youtu.be/dQw4w9WgXcQ)


