![Logo](https://imagizer.imageshack.com/img922/116/AvGc6M.png)
# Coasty Mobile User README

Coasty is a live fish marketing app made to give aquarium enthusiasts and other sea-life connoisseurs a place to care for their own exotic fish. Download the app today and bring your new friend home with you. 

## Installation
Download the files from the git repository through your command line.
```bash
git clone https://Coasty@bitbucket.org/Coasty_app/coasty_app.git
```
Alternatively, you can visit the [unofficial website](https://Coasty-Landing-Page.dariustb.repl.co) to download the binary release. To do this follow these steps:
1. Visit the landing page at [https://Coasty-Landing-Page.dariustb.repl.co](https://Coasty-Landing-Page.dariustb.repl.co).
2. Scroll down to the downloads section of the page (with the phone image and 3 following columns)
3. Click the [download Coasty Mobile App](https://coasty-landing-page.dariustb.repl.co/downloads.html) link under the first bullet. This will lead you to the downloads page.
4. Click the download link of whichever release you would like to download on the release table.

## Usage
![Coastly App](https://imagizer.imageshack.com/v2/640x480q90/924/IGWM37.png)

1. First open the file and wait for it to load.
2. After loading, take the time to appreciate the appearance of the mobile app, because the development is skeletal and virtually non-functioning.
3. Close the application, then take a deep look at your actions that had led you to downloading the file.


## Contributing

Contributions and suggestions for improvements are welcome. For major changes, please contact me via [email](mailto:coasty@protonmail.com) to discuss what you would like to change. However, this project is fictional and I may not respond.

Please make sure to update tests as appropriate.

## Notes

 - The website and documents are fake and are created to fulfill the
   necessary instructions for assignment 4.
  - If you're outside of the class, seeing this, drop a positive comment about the fake app. I might get bonus points

## Credits

This project is completed by Darius Brown for CSC 435 Software Engineering with prof. Cherry.

## License

[(c) 2020 Darius Brown Astley Labs Software License](https://youtu.be/dQw4w9WgXcQ)


